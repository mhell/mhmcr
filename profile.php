﻿<?php
	
	/*
	* MhMCR r01
	* based on WebMCR 1.5
	* by MineHell.ru developers
	*/

	header('Content-Type: text/html;charset=UTF-8');
	require_once('system.php');
	require_once('inc/news.inc.php');
	$cloak = $user->lvl() >= $config['user_level_cloak'];
	$skin = $user->lvl() >= $config['user_level_skin'];
	$static = array();
	$ql = mysql_query("SELECT `name`,`url` FROM ".$db['tables']['static']." ORDER BY `id` DESC;");
	while ($entry = mysql_fetch_assoc($ql))
	{
		if ($entry['url']!='main')
			$static[] = $entry;
	}	
	$opts = array();
	$ql = mysql_query("SELECT * FROM ".$db['tables']['data'].";");
	while ($entry = mysql_fetch_assoc($ql))
	{
		$opts[$entry['property']] = $entry['value'];
	}
	$changed = false;
	$cerror = '';
	if (@$_POST) {
		if (@$_FILES['skin'] && POSTGood('skin') && $skin) {
			POSTUpload('skin', $user->getSkinFName(), 64, 2);
		} else {
			$cerror .= 'Ошибка при загрузке скина. ';
		}
		if (@$_FILES['cloak'] && POSTGood('cloak') && $cloak) {
			POSTUpload('FILE', $user->getCloakFName(), 22, 1.29);
		} else {
			$cerror .= 'Ошибка при загрузке плаща. ';
		}
		if (@$_POST['newpass'] && @$_POST['pass'] && @$_POST['newpass']==@$_POST['newpass2']) {
			$user->changePassword($_POST['newpass'],$_POST['pass'],true);
		} else {
			$cerror .= 'Ошибка при смене пароля. ';
		}
		if (@$_POST['delskin'] && $skin) {
			$user->deleteSkin();
		} else {
			$cerror .= 'Ошибка при удалении скина. ';
		}
		if (@$_POST['delcloak'] && $cloak) {
			$user->deleteCloak();
		} else {
			$cerror .= 'Ошибка при удалении плаща. ';
		}
		$changed = true;
	}
	if (@$_GET['login']) {
		$myprofile = false;
		$userentry = new User(mysql_real_escape_string($_GET['login']),$db['users']['username']);
		$notfound = false;
		if (!$userentry->id()) {
			$notfound = true;
		}
	} else {
		$myprofile = true;
		$userentry = $user;
	}
	include($config['style_dir'].'profile.html');
?>