<?php
	header('Content-Type: text/html;charset=UTF-8');
	require_once('../system.php');
	$news       = '';
	$way_style  = 'style/';
	$page_title = 'Новостная лента';
	if (isset($_GET['l']))
		$curlist = (int) $_GET['l']; //страница
	else
		$curlist = 1;
	if (isset($_GET['id']))
		$spec_new = (int) $_GET['id']; //страница
	else
		$spec_new = -1;
	function arrowsGenerator($link, $curpage, $itemsnum)
	{
		$numoflists = ceil($itemsnum / 10);
		$arrows     = '';
		if ($numoflists > 10 and $curpage > 4) {
			$showliststart = $curpage - 4;
			$showlistend   = $curpage + 5;
			if ($showliststart < 1)
				$showliststart = 1;
			if ($showlistend > $numoflists)
				$showlistend = $numoflists;
		} else {
			$showliststart = 1;
			if ($numoflists < 10)
				$showlistend = $numoflists;
			else
				$showlistend = 10;
		}
		if ($numoflists > 1) {
			if ($curpage > 1)
				$arrows .= '<a href="' . $link . 'l=' . ($curpage - 1) . '"><</a> ';
			for ($i = $showliststart; $i <= $showlistend; $i++)
				$arrows .= '<a href="' . $link . 'l=' . $i . '">' . $i . '</a> ';
			if ($curpage < $numoflists)
				$arrows .= '<a href="' . $link . 'l=' . ($curpage + 1) . '">></a>';
		}
		return $arrows;
	}
	function AddMem($id, $title, $text, $date) //генерация HTML 
	{
		global $news;
		$news .= '<div class="newsItem">
			<p class="title">' . $title . '</p>
			<hr />
			<p>' . $text . '</p>
			<p class="datetime">Добавлено ' . $date . '</p>
		  </div>
		 ';
	}
	/* Для RSS - уникальная ссылка на каждую новость */
	if ($spec_new != -1) {
		$result = mysql_query("SELECT * FROM ".$db['tables']['news']." WHERE id='" . $spec_new . "'");
		if (mysql_num_rows($result) == 1) {
			$line = mysql_fetch_array($result);
			$parts = explode('[!break-short]',$line['content']);
			$line['content'] = $parts[0];
			AddMem($line['id'], $line['name'], html_entity_decode($line['content']), $line['date']);
		}
		include('../inc/serverstate.php');
		require_once($way_style . 'news.html');
		exit;
	}
	// вывод из БД
	$result = mysql_query("SELECT * FROM ".$db['tables']['news']." ORDER by date DESC LIMIT " . (10 * ($curlist - 1)) . ",10");
	if (mysql_num_rows($result) != 0) {
		while ($line = mysql_fetch_array($result)) {
			$parts = explode('[!break-short]',$line['content']);
			$line['content'] = $parts[0];
			AddMem($line['id'], $line['name'], html_entity_decode($line['content']), $line['date']);
		}
		$result  = mysql_query("SELECT COUNT(*) FROM ".$db['tables']['news']);
		$line    = mysql_fetch_array($result);
		$newsnum = $line[0];
		$news .= '<p class="list-pages">' . arrowsGenerator("news.php?", $curlist, $newsnum) . '</p>';
	} else
		$news .= "<h3>Новостная лента пуста...</h3>";
	require_once('../inc/serverstate.php');
	include_once $way_style . 'news.html';
?>