<?php
	require_once('../../system.php');
	$title     = mgetOpt('site-name');
	$desc      = mgetOpt('site-slogan');
	$rss_doc   = '';
	$site_news = mgetOpt('url-base') . $config['mcsrv_dir'] . 'news.php';
	$num_news  = mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM " . $db['tables']['news']));
	if (empty($num_news[0]))
		exit;
	define('DATE_FORMAT_RFC822', 'r');
	Header("content-type: application/rss+xml; charset=utf-8");
	$cur_date = mysql_fetch_row(mysql_query("SELECT DATE_FORMAT(NOW(),'%a, %d %b %Y %T')"));
	$cur_date = $cur_date[0];
	$result   = BD("SELECT * FROM " . $db['tables']['news'] . " ORDER by date DESC LIMIT 0,10");
	if (mysql_num_rows($result) != 0) {
		print '<?xml version="1.0" encoding="UTF-8"?>';
		include 'rss_header.html';
		while ($line = mysql_fetch_array($result)) {
			$name = $line['name'];
			$date = date("r", strtotime($line['date']));
			$link = $site_news . '?id=' . $line['id'];
			$post = strip_tags(html_entity_decode($line['content']));
			include 'rss.html';
		}
		include 'rss_footer.html';
	}
	echo $rss_doc;
?>