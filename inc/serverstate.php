<?php
	require_once("../system.php");
	/*
	Получение состояния сервера по RCON \ Query
	Список игроков \ время прибывания на сервере
	*/
	$update_cashe = 5; //rcon connect period in minutes
	function TimeToNormal($time, $time2 = -1)
	{
		$string = "";
		if ($time2 == -2)
			$time_sec = $time;
		else {
			$cur_time = ($time2 == -1 ? date('Y-m-d H:i:s') : $time2);
			$time_sec = strtotime($cur_time) - strtotime($time);
		}
		if ($time_sec < 0)
			$time_sec = -$time_sec;
		if ($time_sec < 60)
			$string = "меньше минуты";
		$tmp_time = intval($time_sec / 86400);
		if ($tmp_time > 0) {
			$string .= $tmp_time;
			$string .= " д. ";
			$time_sec = $time_sec % 86400;
		}
		$tmp_time = intval($time_sec / 3600);
		if ($tmp_time > 0) {
			$string .= $tmp_time;
			$string .= " ч. ";
			$time_sec = $time_sec % 3600;
		}
		$tmp_time = intval($time_sec / 60);
		if ($tmp_time > 0) {
			$string .= $tmp_time;
			$string .= " мин.";
			$time_sec = $time_sec % 60;
		}
		return $string;
	}
	/*
	Get user list by
	RCON access to the server
	*/
	function GetUserList()
	{
		global $game_server, $rcon_port, $rcon_pass, $rcon_server_stat, $query_port;
		if (empty($game_server))
			return 'offline';
		if ($rcon_server_stat)
			require_once("rcon.class.php");
		else
			require_once("query.function.php");
		if ($rcon_server_stat) {
			try {
				$rcon = new MinecraftRcon;
				$rcon->Connect($game_server, $rcon_port, $rcon_pass);
				$str = $rcon->Command('list');
			}
			catch (MinecraftRconException $e) {
				if ($e->getMessage() == 'Server offline')
					return 'offline';
			}
			$str   = str_replace(array(
				"\r\n",
				"\n",
				"\r"
			), '', $str);
			$names = explode(', ', substr($str, 19));
			if (!empty($names))
				for ($i = 0; $i < sizeof($names); $i++)
					trim($names[$i]);
			if ($names[0] == '')
				$names = false;
			return $names;
		} else {
			$full_state = mcraftQuery($game_server, $query_port);
			if (empty($full_state))
				return 'offline';
			else {
				$system_users                        = $full_state['players'];
				$system_users[sizeof($system_users)] = array(
					$full_state['hostip'],
					$full_state['hostport'],
					$full_state['hostname'],
					$full_state['maxplayers']
				);
				return $system_users;
			}
		}
	}
	function LoadUsersOnline()
	{
		global $user_cache, $bd_aUserID, $bd_aServer, $bd_table_accounts;
		$result = BD("SELECT $bd_aUserID FROM $bd_table_accounts WHERE $bd_aServer IS NOT NULL ORDER by gameplay_last DESC");
		if (!mysql_num_rows($result))
			return false;
		$cUser = NULL;
		while ($line = mysql_fetch_array($result)) {
			$user_online   = new User($line['id'], $bd_aUserID);
			$keyId         = sizeof($cUser);
			$cUser[$keyId] = array(
				$user_online->name(),
				$user_online->gameLoginLast()
			);
		}
		if (empty($cUser))
			return false;
		else
			return $cUser;
	}
	function UpdateUsersOnline()
	{
		global $update_cashe, $bd_aUsername;
		$srv_last_update = explode('|', getGameInfo('state'));
		$time_now        = mysql_fetch_row(BD("SELECT NOW()"));
		if (strtotime($time_now[0]) - strtotime($srv_last_update[1]) < $update_cashe * 60)
			return false;
		$old_userlist = LoadUsersOnline();
		$new_userlist = GetUserList();
		if ($new_userlist == 'offline') {
			updateGameInfo('state', 'offline|' . $time_now[0]);
			return false;
		} else {
			if (is_array($new_userlist[sizeof($new_userlist) - 1])) {
				$spec_key = sizeof($new_userlist) - 1;
				updateGameInfo('state', 'online|' . $time_now[0] . '|' . $new_userlist[$spec_key][0] . '|' . $new_userlist[$spec_key][1] . '|' . $new_userlist[$spec_key][2] . '|' . $new_userlist[$spec_key][3]);
			} else
				updateGameInfo('state', 'online|' . $time_now[0]);
		}
		if ($old_userlist)
			for ($i = 0; $i < sizeof($old_userlist); $i++)
				if (!$new_userlist or !in_array($old_userlist[$i][0], $new_userlist)) {
					$user_logout = new User($old_userlist[$i][0], $bd_aUsername);
					$user_logout->gameLogoutConfirm();
				}
		return true;
	}
	if (isset($_GET["type"]))
		$type = (int) $_GET["type"];
	else
		$type = 3;
	UpdateUsersOnline();
	$server_inf_str = getGameInfo('state');
	if (empty($game_server)) {
		$game_server = 'game_server unconfigured';
		$serv_online = 0;
	} else
		$serv_online = strncmp($server_inf_str, 'offline', 7);
	$server_users_html = '';
	$server_inf_html   = $game_server_str;
	if ($serv_online) {
		$old_userlist = LoadUsersOnline();
		if (!$old_userlist)
			$num_str = 0;
		else
			$num_str = sizeof($old_userlist);
		$server_inf_str = explode('|', $server_inf_str);
		//$server_inf_str[2] -- server ip set in server.properties may be local
		if (!empty($server_inf_str[2]))
			$server_inf_html = $game_server_str . ':' . $server_inf_str[3] . ' (' . $num_str . '/' . $server_inf_str[5] . ')';
		else
			$server_inf_html = $game_server_str . ' (' . $num_str . ')';
	}
	/*
	
	dynamic load for main page 
	
	*/
	if ($type == 1) {
		//ob_start();
		if ($serv_online) {
			print '<h5 class="online">Онлайн '.$num_str.'/'.mgetOpt('server-slots').'</h5>';
		} else {
			print '<h5 class="offline">Оффлайн</h5>';
		}
		/*
		$server_state_html = str_replace(array(
			"\r\n",
			"\n",
			"\r"
		), '', ob_get_clean());
		#echo "<script>parent.document.getElementById('server-state').innerHTML = '" . $server_state_html . "';</script>";
		
		
		full info for news.php
		
		without dynload
		
		*/
	} elseif ($type == 3) {
		ob_start();
		if ($serv_online) {
			$time_now = mysql_fetch_row(BD("SELECT NOW()"));
			if ($num_str) {
				if ($num_str > 10)
					$count = 10; //лаунчер не поддерживает вывод в overflow , просто не выводим, можно так же ограничить кол-во записей в запросе в БД
				else
					$count = $num_str;
				for ($i = 0; $i < $count; $i++)
					$server_users_html .= '<tr><td align="left">' . $old_userlist[$i][0] . '</td><td class="infoPlayers">' . TimeToNormal($old_userlist[$i][1], $time_now[0]) . '</td></tr>';
				if ($num_str > $count)
					$server_users_html .= '<tr><td colspan="2" align="center">и еще ' . ($num_str - $count) . ' ... </td></tr>';
			} else
				$server_users_html .= '<tr><td colspan="2" align="center">сервер пуст</td></tr>';
			include $way_style . 'serverstate_news_online.html';
		} else
			include $way_style . 'serverstate_news_offline.html';
		$server_state_html = ob_get_clean();
	}
?>