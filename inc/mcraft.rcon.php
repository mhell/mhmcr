<?php
require_once('rcon.class.php');
require_once("../system.php");


/*

HTML version of GetUserList

*/

function GetUserListHTML($result) {

$str = trim($result);
$str = str_replace(array("\r\n", "\n", "\r"),'', $str);

$names = explode(', ',substr($str, 19)); 

if (!empty($names)) for($i=0;$i<sizeof($names);$i++) trim($names[$i]); 

if ($names[0]=='') unset($names);

if (empty($names)) return array('<p>Сервер пуст</p>','');

$html = '';
$script = '';

 for($i=0;$i<sizeof($names);$i++) {
 
 $script .= 'parent.addNickButton("'.$names[$i].'",'.$i.');'; 
 $html .= '<p><a href="#" id="nickButton'.$i.'">'.$names[$i].'</a></p>';
 
 }

 
return array($html,$script);
}

if (empty($user) or $user->lvl < 15) exit;
if (empty($game_server)) exit('<script>parent.showResult("rcon unconfigured");</script>');
if (empty($_POST['command']) and empty($_POST['userlist'])) exit('<script>parent.showResult("command is empty");</script>');

	try
	{
		$rcon = new MinecraftRcon;
		$rcon->Connect( mgetOpt('server-ip'), mgetOpt('server-rcon-port'), mgetOpt('server-rcon-pass'));
		
		if (!empty($_POST['userlist'])) {

		 $page = GetUserListHTML($rcon->Command('list'));
		 exit("<script>parent.document.getElementById('users_online').innerHTML = '".$page[0]."'; ".$page[1]."</script>");
		 
		}	
		
		$command = trim($_POST['command']);
		$command = str_replace(array("\r\n", "\n", "\r"),'', $command);
		$command = preg_replace('| +|', ' ', $command);
		 
		 
		$str = trim(htmlspecialchars($rcon->Command($command), ENT_QUOTES ));

		$str = str_replace(array("\r\n", "\n", "\r"),'', $str);

		if (!strncmp($command,'say',3) and strlen($str) > 2) $str = substr($str, 2);
		if (!strncmp(substr($str, 2),'Usage',5)) $str = substr($str, 2);
		 
		$str = str_replace(array(chr(167)), '', $str); 
		
		echo '<script>parent.showResult("'.$str.'");</script>';

	}
	catch( MinecraftRconException $e )
	{
		echo '<script>parent.showResult("'.$e->getMessage( ).'");</script>'; 
	}

$rcon->Disconnect( );


?>