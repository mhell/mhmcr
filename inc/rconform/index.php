<?php require_once("../../system.php"); if (empty($user) or $user->lvl < 15) exit; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Minecraft RCON v1.0b by NC22</title>

<style>
* {margin: 0px; padding: 0px;}

body {
background: #fff url(bg0.png) repeat top left;
font-size: 16px;
font-family: Cambria, Palatino, "Palatino Linotype", "Palatino LT STD", Georgia, serif;
-webkit-font-smoothing: antialiased;
-moz-font-smoothing: antialiased;
font-smoothing: antialiased;
}

* html body {height: 100%;}

a{
	color: #eaf0f6;
	text-decoration: none;
}

.console_holder{
	margin: 0 auto;
	margin-top: 20px;
    padding: none;
    height: auto;
	width: 80%;
}

.help_block {

    display: block;
    clear: both;
	float:right;	
	margin-left: 8px;
    text-align: left;
	color:#e9e9e9;
	background: #929292;
    width: 250px;
	max-height: 600px;
	overflow:auto;
    border: 2px dashed #747474;
}

.main_left_block{

  background: #929292;
  color: #e5e5e5;
  text-align: center;
  border: 2px dashed #747474;
  padding: 4px;
  margin-bottom: 10px;
  
}

.main_left_block #console_log {
  
  text-align: left;
  height: 300px;
  font-size: 14px;
  overflow:auto;
  background:#676767;
}

</style>

<!--[if IE]>
<style>
.console_holder{
	position:absolute;
	left:10%;
}

.main_left_block{
margin-right:260px;
}

</style>
<![endif]-->

</head>

<body>
<script type='text/javascript'>
var sendBeasy = false;

function _getTime() {
	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	
	if (minutes < 10){
		minutes = "0" + minutes;
	}
	return hours + ":" + minutes;
}

function showResult(result) {
	document.getElementById('console_log').innerHTML += '<p>[' + _getTime() + '] ' + result + '</p>'
}

function addNickButton(name,id) {

var tmp = document.getElementById('nickButton'+id) 
  if (tmp != null) tmp.onclick = function(){
  
	document.command_form.command.value += ' '+name
		
	return false
  }

}

function cType(com) {

document.command_form.command.value += com + ' '

return false

}

function cShow(m) {

if (m==1) {
document.getElementById('help_buttons').innerHTML = '<b>Игроки</b> | <a href="#" onclick="cShow(2)">Команды</a>'
document.getElementById('commands-list').style.display = 'none'
document.getElementById('users-list').style.display = 'block'
} else if (m==2) {
document.getElementById('help_buttons').innerHTML = '<a href="#" onclick="cShow(1)">Игроки</a> | <b>Команды</b>'
document.getElementById('commands-list').style.display = 'block'
document.getElementById('users-list').style.display = 'none'
}

return false
}

window.onload = function () {
var tmp = document.getElementById('command_send')
  if (tmp != null) tmp.onclick = function(){
	
	if (sendBeasy) return false

	if (document.command_form.command.value.length == 0) return false
	
	var log = document.getElementById("console_log")
		log.innerHTML += '<p>[' + _getTime() + '] ' + document.command_form.command.value + '</p>'
		log.scrollTop = log.scrollHeight
	
	document.command_form.submit();	
	document.command_form.command.value = ''	

	sendBeasy = true
	
	document.getElementById('systemFrame01').onload = function(){

		sendBeasy = false
		var log = document.getElementById("console_log")
		log.scrollTop = log.scrollHeight
		
	}
  }
  
  tmp = document.command_form.command 
  if (tmp != null) tmp.onkeyup = function(event){
  
	if ( this.value.length == 0 || sendBeasy) return false
	
	if ( event.keyCode == 13 ) document.getElementById('command_send').onclick()
  }
  
  tmp = document.getElementById('users_online_refresh') 
  if (tmp != null) tmp.onclick = function(){
  
		document.getElementById('users_online_refresh').disabled = !document.getElementById('users_online_refresh').disabled
		
		document.userlist_form.submit();
			
		document.getElementById('systemFrame01').onload = function(){

		document.getElementById('users_online_refresh').disabled = !document.getElementById('users_online_refresh').disabled
		
		}		
  }
}	
</script>

<h1 style="color: #e5e5e5;"> Minecraft RCON session </h1>

<div class="console_holder">

    <div class="help_block">
        <div style="background:#747474; text-align:center; padding:2px;" id="help_buttons"><a href="#" onclick="cShow(1)">Игроки</a> | <b>Команды</b></div>
       
		<div style="text-align:center; display: none;" id="users-list">
		<div><input type="button" value="Обновить" id="users_online_refresh" /></div>
		<div id="users_online"></div>
        </div>
	    <div style="text-align:left;" id="commands-list">
			<p>Список комманд</p>
			
			<p><a href="#" onclick="cType('tell')">tell [playername] [message]</a></p>
			<p><a href="#" onclick="cType('me')">me [actiontext]</a></p>
			<p><a href="#" onclick="cType('kill')">kill</a></p>
			<p><a href="#" onclick="cType('ban')">ban [playername]</a></p>
			<p><a href="#" onclick="cType('ban-ip')">ban-ip [ip-address]</a></p>
			<p><a href="#" onclick="cType('banlist')">banlist</a></p>
			<p><a href="#" onclick="cType('deop')">deop [playername]</a></p>
			<p><a href="#" onclick="cType('gamemode')">gamemode [playername] 0/1</a></p>
			<p><a href="#" onclick="cType('give')">give [player] [item] [amount]</a></p>
			<p><a href="#" onclick="cType('help')">help</a></p>
			<p><a href="#" onclick="cType('kick')">kick [playername]</a></p>
			<p><a href="#" onclick="cType('list')">list</a></p>
			<p><a href="#" onclick="cType('op')">op [playername]</a></p>
			<p><a href="#" onclick="cType('pardon')">pardon [playername]</a></p>
			<p><a href="#" onclick="cType('pardon-ip')">pardon-ip [ip-address]</a></p>
			<p><a href="#" onclick="cType('save-all')">save-all</a></p>
			<p><a href="#" onclick="cType('save-off')">save-off</a></p>
			<p><a href="#" onclick="cType('save-on')">save-on</a></p>
			<p><a href="#" onclick="cType('say')">say [message]</a></p>
			<p><a href="#" onclick="cType('stop')">stop</a></p>
			<p><a href="#" onclick="cType('time')">time [set|add] [number]</a></p>
			<p><a href="#" onclick="cType('toggledownfall')">toggledownfall</a></p>
			<p><a href="#" onclick="cType('tp')">tp [playername] [targetplayer]</a></p>
			<p><a href="#" onclick="cType('whitelist')">whitelist</a></p>
			<p><a href="#" onclick="cType('whitelist reload')">whitelist reload</a></p>
			<p><a href="#" onclick="cType('xp')">xp [playername] [amount]</a></p>
			<p><a href="#" onclick="cType('defaultgamemode')">defaultgamemode [2/1/0]</a></p>
			
        </div>
    </div>
	
    <div style="overflow: hidden;">


		<div class="main_left_block">
			
			<div style="background:#747474; text-align:center;"> Лог консоли</div>
		
			<div id="console_log">

			
			</div>
			<div style="background:#747474; text-align:center;">Ввод команды</div>
			
			<div style="background:#818181; text-align:center; padding: 5px;">
						<form action="../mcraft.rcon.php" method="POST" name="command_form" target="systemFrame">
						
							<p><input name="command" type="text" style="width: 80%;">&nbsp;<a href="#" id="command_send" style="border: 2px solid #363636;">SEND</a></p>
							
						</form>
							
			</div>
		
		
		</div>
    

    </div>



</div>

<iframe name="systemFrame" id="systemFrame01" src="" style="display: none;"></iframe>

<form action="../mcraft.rcon.php" method="POST" name="userlist_form" target="systemFrame" style="display: none;">
	<input type="hidden" name="userlist" value="1" />						
</form>

</body>

</html>