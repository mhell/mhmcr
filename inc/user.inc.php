<?php

class User {
	var $id;
	var $name;
	var $lvl;
	var $tmp;
	var $ip;
	var $email;

	function User($input,$method) {
		global $db;
		$result = mysql_query("SELECT ".$db['users']['username'].",".$db['users']['id'].",".$db['users']['level'].",".$db['users']['temp'].",".$db['users']['ip'].",".$db['users']['email']." FROM ".$db['tables']['users']." WHERE $method='$input'");
		if (!$result) 
			$this->id=false;
		else {
			$line = mysql_fetch_array($result, MYSQL_ASSOC);
			$this->id = $line[$db['users']['id']];			
			$this->name = $line[$db['users']['username']];			
			$this->lvl = $line[$db['users']['level']];
			$this->tmp = $line[$db['users']['temp']];
			$this->ip = $line[$db['users']['ip']];
			$this->email = $line[$db['users']['email']];
		}
		return true;			
	}
	
	function gameLoginConfirm() {
		global $db;
		if (!$this->id) 
			return false;
		mysql_query("UPDATE ".$db['tables']['users']." SET ".$db['users']['lastlogin']."=NOW() WHERE ".$db['users']['id']."='".intval($this->id)."' LIMIT 1;"); 
		return true;
	}
	
	function gameLogoutConfirm() {
		global $db;
		if (!$this->id) 
			return false;
		$result = mysql_query("SELECT ".$db['users']['id']." FROM ".$db['tables']['users']." WHERE ".$db['users']['server']." IS NOT NULL and ".$db['users']['id']."='".intval($this->id)."' LIMIT 1;");
		if ($result) //(mysql_num_rows($result) == 1)
			mysql_query("UPDATE ".$db['tables']['users']." SET ".$db['users']['server']."=NULL WHERE ".$db['users']['id']."='".intval($this->id)."'"); 
		return true;
	}
	
	function gameLoginLast() {
		global $db;
		if (!$this->id) return false;
		$result = mysql_query("SELECT ".$db['users']['lastlogin']." FROM ".$db['tables']['users']." WHERE ".$db['users']['lastlogin']."<>'0000-00-00 00:00:00' and ".$db['users']['id']."='".intval($this->id)."' LIMIT 1;");
		if ($result) {
			return mysql_result($result,0);	
		} else {
			return false;
		}
	}
	
	function login($tmp,$ip) {
		global $db;
	
		if (!$this->id) return false;
	
		mysql_query("UPDATE ".$db['tables']['users']." SET ".$db['users']['temp']."='$tmp' WHERE ".$db['users']['id']."='".$this->id."'");
		mysql_query("UPDATE ".$db['tables']['users']." SET ".$db['users']['ip']."='".GetRealIp()."' WHERE ".$db['users']['id']."='".$this->id."'"); 
	
		$this->tmp = $tmp;
		
		return true;
	}
	
	function logout() {
		global $db;
		if (!isset($_SESSION)) session_start();
		if (isset($_SESSION)) session_destroy();
		if (isset($_COOKIE['PRTCookie1']))
		{ 
			$cook=$_COOKIE['PRTCookie1'];
			mysql_query("UPDATE ".$db['tables']['users']." SET ".$db['users']['temp']."='0' WHERE ".$db['users']['temp']."='$cook'");
			setcookie("PRTCookie1","",time()-3600);
			$this->tmp = 0;
		}	
	}
	
	function name()
	{
		return $this->name;
	}
	
	function email()
	{
		return $this->email;
	}
	
	function getSkinFName() {
		global $config;
		return $config['mcsrv_dir'].$config['skins_dir'].$this->name.'.png';
	}
	
	function getCloakFName() {
		global $config;
		return $config['mcsrv_dir'].$config['cloaks_dir'].$this->name.'.png';
	}
	
	function deleteSkin() {
		if (file_exists($this->getSkinFName())) {
			unlink($this->getSkinFName());	
			$this->deleteBuffer();
		}
	}
	
	function deleteCloak() {
		if (file_exists($this->getCloakFName())) {
			unlink($this->getCloakFName());	
			$this->deleteBuffer();
		}
	}
	
	function deleteBuffer() {
		global $config;

		$mini = $config['mcsrv_dir'].'tmp/skin_buffer/'.$this->name.'_Mini.png';
		$skin = $config['mcsrv_dir'].'tmp/skin_buffer/'.$this->name.'.png';

		if (file_exists($mini)) 
			unlink($mini); 
		if (file_exists($skin)) 
			unlink($skin); 
	}
	
	function changeName($newname) {
		global $db,$config;
		if (!$this->id) 
			return false;
		$newname = addslashes(trim($newname));
		if (!preg_match("/^[a-zA-Z0-9_-]+$/", $newname)) 
			return false;
		$result = mysql_query("SELECT ".$db['users']['username']." FROM ".$db['tables']['users']." WHERE ".$db['users']['username']."='$newname'");
		if (mysql_num_rows($result)) 
			return false;
		if ((strlen($newname) < 4) or (strlen($newname) > 8)) 
			return false;
		mysql_query("UPDATE ".$db['tables']['users']." SET ".$db['users']['username']."='$newname' WHERE ".$db['users']['username']."='".$this->name."'");
		if (!empty($_SESSION['user_name']) and $_SESSION['user_name'] == $this->name) 
			$_SESSION['user_name'] = $newname;
		/* �������������� ����� ����� � ����� */
		$way_tmp_old = $config['mcsrv_dir'].$config['skins_dir'].$this->name.'.png';
		$way_tmp_new = $config['mcsrv_dir'].$config['skins_dir'].$newname.'.png';
		if (file_exists($way_tmp_old) and !file_exists($way_tmp_new)) 
			rename($way_tmp_old, $way_tmp_new);
		$way_tmp_old = $config['mcsrv_dir'].$config['skins_dir'].$this->name.'.png';
		$way_tmp_new = $config['mcsrv_dir'].$config['skins_dir'].$newname.'.png';
		if (file_exists($way_tmp_old) and !file_exists($way_tmp_new)) 
			rename($way_tmp_old, $way_tmp_new);
		$this->name = $newname;
		return true;
	}
	
	function changePassword($newpass,$pass = '',$check = false) {
		global $db;
		include_once('inc/pass.inc.php');
		if (!$this->id) 
			return 0;
		if ($check) {
			if (strlen($newpass)<6)
				return 13;
			$result = mysql_query("SELECT ".$db['users']['password']." FROM ".$db['tables']['users']." WHERE ".$db['users']['username']."='".$this->name."'"); 
			$line = mysql_fetch_array( $result );
			if (!$line or !checkPass($line[$db['users']['password']],$pass)) 
				return 12;
		}
		mysql_query("UPDATE ".$db['tables']['users']." SET ".$db['users']['password']."='".createPass($newpass)."' WHERE ".$db['users']['username']."='".$this->name."'"); 
		return 15;
	}
	
	function changeLvl($newlvl) {
		global $db;
		$newlvl = (int) $newlvl;
		if ($newlvl < 0 or $newlvl > 15)
			return false;
		mysql_query("UPDATE ".$db['tables']['users']." SET ".$db['users']['level']."='$newlvl' WHERE ".$db['users']['username']."='".$this->name."'"); 
		return true;
	}
	
	function changeEmail($newemail) {
		global $db;
		if (!filter_var($newemail,FILTER_VALIDATE_EMAIL))
			return false;
		mysql_query("UPDATE ".$db['tables']['users']." SET ".$db['users']['email']."='$newemail' WHERE ".$db['users']['username']."='".$this->name."'"); 
		return true;
	}
	
	function id()
	{
		return $this->id;
	}
	
	function lvl()
	{
		return $this->lvl;
	}	
	
	function tmp()
	{
		return $this->tmp;
	}
	
	function ip()
	{
		return $this->ip;
	}	
}