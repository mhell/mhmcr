<?php

/* Параметры MySQL базы данных */

$bd='mhmcr'; //название БД
$bd_host = 'localhost'; // адрес сервера MySQL если нужно указать порт то адрес:порт
$bd_name = 'root';      // имя 
$bd_pass = '';          // пароль

/* Некоторые параметры */

$system_log = false;         // ЛОГ авторизации. ведется в $way_mcraft. [true] если активен [false] - если выключен
$skin_size = 20;             // максимальный размер файла (скины и плащи) в кб 
$next_registration_time = 1; // бан по IP на повторную регистрацию в часах - 0 выключить баны. 
$news_main = true;           // новости на странице пользователя [true] включены [false] - если выключены

/* Параметры авторизации RCON (состояние сервера \ админ панель) */

$game_server = "localhost";           // адрес вашего Bukkit \ официального сервера, используется при подключении
$game_server_str = "123.123.123.123"; // строка выводимая в статусе сервера

$rcon_server_stat = false; // использовать RCON вместо Query для получения состояния сервера [true] RCON [false] Query
$query_port = 25565; // порт для Query, если активен

$rcon_port = 25575;  // порт для RCON, если активен
$rcon_pass = "12345";

$bd_table_news = 'mcr_news'; //таблица новостей выводимых в лаунчере \ на сайте
$bd_table_accounts = 'mcr_accounts'; //таблица с акками пользователей в игре 

// поля ACCOUNTS для быстрого изменения под определенный плагин, базу 
	
$bd_aUserID = 'id';
$bd_aUsername = 'login'; 
$bd_aPassword = 'password';
$bd_aSession = 'session';
$bd_aServer = 'server';	
$bd_aLvl = 'lvl';
$bd_aTmp = 'tmp';
$bd_aIP = 'ip';

$bd_table_data = 'mcr_data';	// версия и билд
$bd_table_ipban = 'mcr_ipban';	// регистрация с ip содержащихся в данной таблице будет недоступна заданое время

/*
 Права пользователей 0 = Заблокированный пользователь | 1 = Пользователь после регистрации на сайте | 15  = Администратор (открывается доступ к панели управления) 
 для входа на сайте \ в игре нужны права > 0
 */

$user_lvl_skin = 1;  // для смены скина
$user_lvl_login = 1; // для смены ника
$user_lvl_pass = 1;  // для изменения пароля
$user_lvl_cloak = 5; // плаща
$user_lvl_news = 9;  // добавления новостей

/* Пути до папок */

$way_style = "templates/";               // путь до папки с темой оформления сайта
$way_mcraft = "mcsrv/";       // путь до папки с web частью сервера minecraft'а ( папка со скинами, плащами, скрипты авторизации... )
$way_skins = "MinecraftSkins/";      // подпапка скинов в папке $way_mcraft
$way_cloaks = "MinecraftCloaks/";    // подпапка плащей
$way_distrib = "MinecraftDownload/"; // подпапка обновлений 

$db_prefix = 'mcr';

$config = array(
		'db_prefix' => 'mcr',
		'style_dir' => 'templates/',
		'mcsrv_dir' => 'mcsrv/',
		'skins_dir' => 'MinecraftSkins/',
		'cloaks_dir' => 'MinecraftCloaks/',
		'distr_dir' => 'MinecraftDownload/',
		'user_level_skin' => 1,
		'user_level_cloak' => 5,
		'user_level_comment' => 1,
	);

$db = array(
		'prefix' => $db_prefix,
		'tables' => array(
			'news' => $db_prefix.'_news',
			'static' => $db_prefix.'_static',
			'data' => $db_prefix.'_data',
			'ipban' => $db_prefix.'_ipban',
			'users' => $db_prefix.'_accounts',
		),
		'users' => array(
			'id' => 'id',
			'username' => 'login',
			'email' => 'email',
			'password' => 'password',
			'session' => 'session',
			'server' => 'server',
			'level' => 'lvl',
			'temp' => 'tmp',
			'ip' => 'ip',
			'lastlogin' => 'gameplay_last',
		),
	);
	
function BD($query) {
    $result = mysql_query( $query );
    return $result;
}

$link = mysql_connect($bd_host, $bd_name, $bd_pass) or die("Логин или пароль для базы неверный");
mysql_select_db($bd,$link) or die("Имя бд указано неверно");
mysql_query ("set character_set_client='utf8'"); 
mysql_query ("set character_set_results='utf8'"); 
mysql_query ("set collation_connection='utf8_general_ci'"); 
$skin =  mysql_fetch_row(mysql_query("SELECT `value` FROM ".$db['tables']['data']." WHERE `property`='site-template' LIMIT 1;"));
$way_style .= $skin[0]."/";
?>