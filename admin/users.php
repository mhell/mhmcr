<?php
	
	/*
	* MhMCR r01
	* based on WebMCR 1.5
	* by MineHell.ru developers
	*/

	if (!defined('mhmcr'))
		die('');
	
	switch (@$_GET['act']) {
		case 'search': {
			$what = @$_GET['what'];
			if (!@$_GET['query']) {
				include('template/usersearch.html');
				break;
			} else {
				if (array_key_exists($_GET['what'],$db['users'])) 
					$field = $db['users'][$_GET['what']];
				else
					$field = $db['users']['username'];
				$qi = mysql_query("SELECT * FROM ".$db['tables']['users']." WHERE ".$field." LIKE '%".mysql_real_escape_string($_GET['query'])."%' ORDER BY ".$db['users']['username']." ASC;");
				$users = array();
				while ($entry = mysql_fetch_assoc($qi)) {
					$users[] = $entry;
				}
				$usercount = count($users);
				$pagecount = ceil($usercount/20);
				$userblocks = array();
				for ($i=0; $i<$pagecount; $i++) {
					$block = array();
					for ($j=0; $j<=19; $j++) {
						$ind = ($i*20)+$j;
						if (array_key_exists($ind,$users))
							$block[]=$users[$ind];
					}
					$userblocks[] = $block;
				}
				include('template/usersearch.html');
				break;
			}
		}
		case 'ban': {
			if (!@$_POST) {
				$userentry = new User(intval($_GET['id']),$db['users']['id']);
				if (!$userentry->id()) {
					Header('Location: ?page=users');
					die('');
				}
				include('template/userban.html');
				break;
			} else { 
				$userentry = new User(intval($_GET['id']),$db['users']['id']);
				if (!$userentry->id()) {
					Header('Location: ?page=users');
					die('');
				}
				if ($_POST['banip']) {
					$days = intval($_POST['days']);
					mysql_query("DELETE FROM ".$db['tables']['ipban']." WHERE IP='".$userentry->ip()."'");
					mysql_query("INSERT INTO ".$db['tables']['ipban']." (IP,time_start,ban_until) VALUES ('".$userentry->ip()."',NOW(),NOW()+INTERVAL ".$days." DAY);");
				}
				if ($_POST['banuser']) {
					$userentry->changeLvl(0);
				}
				Header('Location: ?page=users');
			}
			break;
		}
		case 'edit': {
			if (!@$_POST) {
				$userentry = mysql_fetch_assoc(mysql_query("SELECT * FROM ".$db['tables']['users']." WHERE ".$db['users']['id']."='".intval($_GET['id'])."' LIMIT 1;"));
				include('template/useredit.html');
				break;
			} else {
				//POSTED!
				$userentry = new User(intval($_GET['id']),$db['users']['id']);
				if (!$userentry->id()) {
					Header('Location: ?page=users');
					die('');
				}
				if (!empty($_POST['username']))
					$userentry->changeName($_POST['username']);
				if (!empty($_POST['email']))
					$userentry->changeEmail($_POST['email']);
				if (!empty($_POST['password']))
					$userentry->changePassword($_POST['password']);
				if (isset($_POST['level'])) 
					$userentry->changeLvl((int) $_POST['level']);
				if (empty($_FILES['skin']['tmp_name']) and !empty($_POST['delskin'])) 
					$userentry->deleteSkin();
				if (empty($_FILES['cloak']['tmp_name']) and !empty($_POST['delcloak']))
					$userentry->deleteCloak();
				if (!empty($_FILES['skin']['tmp_name']))
					if (POSTGood('skin')) 
						POSTUpload('skin', $userentry->getSkinFName(), 64, 2);
				if (!empty($_FILES['cloak']['tmp_name']))
					if (POSTGood('cloak')) 
						POSTUpload('cloak', $userentry->getCloakFName(), 22, 1.29);
				Header('Location: ?page=users');
				break;
			}
		}
		case 'del': {
			$id = intval($_GET['id']);
			$userentry = new User($id,$db['users']['id']);
			if (!$userentry->id()) {
				Header('Location: ?page=users');
				die('');
			}
			$userentry->deleteSkin();
			$userentry->deleteCloak();
			mysql_query("DELETE FROM ".$db['tables']['users']." WHERE ".$db['users']['id']."='".$id."' LIMIT 1;");
			Header('Location: ?page=users');
			break;
		}
		default: {
			$qi = mysql_query("SELECT * FROM ".$db['tables']['users']." ORDER BY ".$db['users']['username']." ASC;");
			$users = array();
			while ($entry = mysql_fetch_assoc($qi)) {
				$users[] = $entry;
			}
			$usercount = count($users);
			$pagecount = ceil($usercount/20);
			$userblocks = array();
			for ($i=0; $i<$pagecount; $i++) {
				$block = array();
				for ($j=0; $j<=19; $j++) {
					$ind = ($i*20)+$j;
					if (array_key_exists($ind,$users))
						$block[]=$users[$ind];
				}
				$userblocks[] = $block;
			}
			include('template/users.html');
			break;
		}
	}