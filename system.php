<?php
require_once('bd_con.php');
/*

Файл - system.php

Системные функции - генерация пароля, проверка пароля и др.

Aвторизация пользователя

*/
include('inc/user.inc.php');
/*

Системные функции

*/

function tmp_name($folder,$pre = '',$ext = 'tmp'){
      $name  = $pre.time().'_';
	  
      for ($i=0;$i<8;$i++) $name .= chr(rand(97,121));
	  
      $name .= '.'.$ext;
	  
      return (file_exists($folder.$name))? tmp_name($folder,$pre,$ext):$name;
}

function ratio($file,$baze = 64, $prop = 2) {
	$input_size = @getimagesize($file);
	if (empty($input_size)) return false;
	if (round($input_size[0] / $input_size[1], 2) != round($prop,2)) return false;
	else if ($input_size[0] < $baze) return false;
	$mp = $input_size[0] / $baze;
	return $mp;
}

function POSTGood($post_name, $format = 'png') {
if ( empty($_FILES[$post_name]['tmp_name']) or 
     $_FILES[$post_name]['error'] != UPLOAD_ERR_OK or
	 !is_uploaded_file($_FILES[$post_name]['tmp_name']) or 
	 substr($_FILES[$post_name]['name'], 1 + strrpos($_FILES[$post_name]['name'], ".")) != $format
   ) return false;
   
return true;
}

function POSTUpload($post_name, $final_way, $baze = 64, $prop = 2) {
	global $config,$user;
	if (!is_dir($config['mcsrv_dir'].'tmp/')) 
		mkdir($config['mcsrv_dir'].'tmp/', 0777); 
	$tmp_file = $config['mcsrv_dir'].'tmp/'.tmp_name($config['mcsrv_dir'].'tmp/');
	if (!move_uploaded_file( $_FILES[$post_name]['tmp_name'], $tmp_file )) 
		exit ('[Ошибка модуля загрузки] Убедитесь, что папка "'.$config['mcsrv_dir'].'tmp/ " доступна для ЗАПИСИ');
	$fsize = ceil($_FILES[$post_name]['size'] / 1024);
	if ( intval(mgetOpt('skin-size')) < $fsize ) { 
		unlink($tmp_file); 
		return 1;
	}
	$input_ratio = ratio($tmp_file, $baze, $prop);
	if (!$input_ratio or $input_ratio > intval(mgetOpt('max-ratio'))) {
		unlink($tmp_file); 
		return 6;
	}
	if (file_exists($final_way)) 
		unlink($final_way);
	rename($tmp_file, $final_way );
	chmod($final_way,0777);
	$user->deleteBuffer();
	return 3;
}

function generateSessionId(){
    // generate rand num
    srand(time());
    $randNum = rand(1000000000, 2147483647).rand(1000000000, 2147483647).rand(0,9);
    return $randNum;
}

function updateGameInfo($type,$value){
	global $db;
    switch($type){
		case 'build': $query = mysql_query("UPDATE ".$db['tables']['data']." SET value='$value' WHERE property = 'latest-game-build'"); break;
		case 'launcher': $query = mysql_query("UPDATE ".$db['tables']['data']." SET value='$value' WHERE property = 'launcher-version'"); break;
		case 'state': $query = mysql_query("UPDATE ".$db['tables']['data']." SET value='$value' WHERE property = 'server-state'"); break;
	}
}

function getGameInfo($type){
	global $db;
    switch($type){
		case 'build':
			$query = mysql_query("SELECT * FROM ".$db['tables']['data']." WHERE property = 'latest-game-build'");    
			$resource = mysql_fetch_array($query);
			return $resource['value'];
			break;
		case 'launcher':
			$query = mysql_query("SELECT * FROM ".$db['tables']['data']." WHERE property = 'launcher-version'"); 
			$resource = mysql_fetch_array($query);
			return $resource['value'];
			break;
		case 'state':
			$query = mysql_query("SELECT * FROM ".$db['tables']['data']." WHERE property = 'server-state'"); 
			$resource = mysql_fetch_array($query);
			return $resource['value'];
			break;
	}
}

function GetRealIp(){
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
		$ip=$_SERVER['HTTP_CLIENT_IP']; 
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	else 
		$ip=$_SERVER['REMOTE_ADDR'];
	return $ip;
}

function CanRegister() {
	global $db;
	$ip = $_SERVER['REMOTE_ADDR']; //если нужен реальный адрес пользователя используйте GetRealIp
	$result = mysql_query("SELECT ban_until FROM ".$db['tables']['ipban']." WHERE IP='$ip'"); 
	$line = mysql_fetch_array($result);
	if ( ($line != NULL) and ($line['ban_until']!='0000-00-00 00:00:00') )
	{
		mysql_close( $link );
		return false;
	}
	return true;
}

function RefreshBans() {
	global $MCR_CONFIG,$db;
	mysql_query("DELETE FROM ".$db['tables']['ipban']." WHERE (ban_until='0000-00-00 00:00:00') AND (time_start<NOW()-INTERVAL ".mgetOpt('ban-ip')." HOUR)");
	mysql_query("DELETE FROM ".$db['tables']['ipban']." WHERE (ban_until<>'0000-00-00 00:00:00') AND (ban_until<NOW())");					
}

function mgetOpt($param) {
	global $db;
	return mysql_result(mysql_query("SELECT `value` FROM ".$db['tables']['data']." WHERE `property`='".mysql_real_escape_string($param)."' LIMIT 1;"),0);
}

function vtxtlog($string) {
	if (!mgetOpt('system-log')) 
		return;
	$log_file = "log.txt";
	if (file_exists($log_file) and round(filesize ($log_file) / 1048576) >= 50) 
		unlink($log_file);
	$fp=fopen($log_file,"a");
	fwrite($fp,$string.PHP_EOL); 
	fclose($fp);
}

/* Проверяем сессию на сайте */
$user = false;
if (!session_id() and isset($_GET['sid'])) 
	session_id($_GET['sid']);		
if (!isset($_SESSION))
	session_start();
if (isset($_SESSION['user_name']) AND $_SESSION['ip'] == $_SERVER['REMOTE_ADDR']) {
	$user = new User($_SESSION['user_name'],$db['users']['username']);
	if (!$user->id()) 
		unset($user);
} 
if (isset($_COOKIE['PRTCookie1']) and empty($user)) { 
	$user = new User($_COOKIE['PRTCookie1'],'tmp');
	if (!$user->id()) {
		unset($user);
		setcookie("PRTCookie1","",time(), '/');
	} else {
		if (!isset($_SESSION)) session_start();
		$_SESSION['user_name'] = $user->name();
		$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
	}
}

if (!empty($user)) {
	if (!strncmp($user->tmp(),'ipcheck_',8) and GetRealIp() != $user->ip()) 
		unset($user);
	elseif ($user->lvl() <= 0) {
		$user->logout(); 
		unset($user);
	}
}

$config['style_dir'] .= mgetOpt('site-template').'/';

?>