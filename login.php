<?php
require_once('system.php');
include_once('inc/pass.inc.php');
function randString( $pass_len = 50 )
{
    $allchars = "abcdefghijklmnopqrstuvwxyz0123456789";
    $string = "";
    mt_srand( (double) microtime() * 1000000 );
    for ( $i=0; $i<$pass_len; $i++ )
	$string .= $allchars{ mt_rand( 0, strlen( $allchars )-1 ) };
    return $string;
}

if (isset($_POST['login']) and empty($user)) {
	$name=mysql_real_escape_string($_POST['login']);
	$pass=mysql_real_escape_string($_POST['password']);
	$result = mysql_query("SELECT ".$db['users']['password']." FROM ".$db['tables']['users']." WHERE ".$db['users']['username']."='$name'"); 
	$line = mysql_fetch_array( $result ); 
	if ($line == NULL or !checkPass($line[$db['users']['password']],$pass)) {
		mysql_close( $link );
		header("Location: ".mgetOpt('url-base')); 
		exit;
	}
	$user = new User($name,$db['users']['username']);
	if ($user->lvl() <= 0) {
		unset($user);
		mysql_close( $link );
		header("Location: ".mgetOpt('url-base')."?e=1"); 
		exit; 
	}
	if (!isset($_SESSION)) 
		session_start();
	$tmpID = randString( 15 );
	if (isset($_POST['ipcheck'])) 
		$tmpID = 'ipcheck_'.$tmpID;
	setcookie( "PRTCookie1", "$tmpID",time() + 60 * 60 * 24 * 30 * 12, '/');
	$user->login($tmpID,GetRealIp());
	$_SESSION['user_id'] = $user->id();
	$_SESSION['user_name'] = $user->name();
	$_SESSION['ip'] = $user->ip();
	header("Location: ".mgetOpt('url-base'));
}

if (isset($_GET['out']) and $user) {
	$user->logout();
	header("Location: ".mgetOpt('url-base'));
	exit;
}
?>