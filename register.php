<?php
	header('Content-Type: text/html;charset=UTF-8');
	require_once('system.php');
	
	if (!$_POST) {
		//Page generation
		
		$static = array();
		$ql = mysql_query("SELECT `name`,`url` FROM ".$db['tables']['static']." ORDER BY `id` DESC;");
		while ($entry = mysql_fetch_assoc($ql))
		{
			if ($entry['url']!='main')
				$static[] = $entry;
		}
		
		$opts = array();
		$ql = mysql_query("SELECT * FROM ".$db['tables']['data'].";");
		while ($entry = mysql_fetch_assoc($ql))
		{
			$opts[$entry['property']] = $entry['value'];
		}
		
		include_once $config['style_dir'].'register.html';
	} else {
		//Checking input
		$email = $_POST['email'];
		$pass = $_POST['pass'];
		$pass2 = $_POST['pass2'];
		$login = $_POST['login'];
		
		$flags = array('pass'=>false,'email'=>false,'login'=>false,'captcha'=>false,'hit'=>false,'canreg'=>false);
		
		if (!preg_match("/^(?=.{1,30}$)[a-zA-Z][a-zA-Z0-9]*$/",$login)) {
			$flags['hit'] = true;
			$flags['login'] = true;
		}
		
		if (!CanRegister()) {
			$flags['hit'] = true;
			$flags['canreg'] = true;
		}
		
		if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
			$flags['hit'] = true;
			$flags['email'] = true;
		}
		
		if (strlen($pass)<6 or $pass!=$pass2) {
			$flags['hit'] = true;
			$flags['pass'] = true;
		}
		
		if (empty($_SESSION['captcha']) || trim(strtolower($_POST['captcha'])) != $_SESSION['captcha']) {
			$flags['hit'] = true;
			$flags['captcha'] = true;
		}
		
		//Checking done
		if (!$flags['hit']) {
			include_once('inc/pass.inc.php');
			$hash = createPass($pass);
			$login = mysql_real_escape_string($login);
			$email = mysql_real_escape_string($email);
			mysql_query("INSERT INTO ".$db['tables']['users']." (".$db['users']['username'].",".$db['users']['password'].",".$db['users']['ip'].") VALUES('".$login."','".$hash."','".GetRealIp()."');");
			if ($ipban = mgetOpt('ip-ban') && $ipban) {
				mysql_query("INSERT INTO ".$db['tables']['users']." (IP,time_start,ban_until) VALUES ('".$_SERVER['REMOTE_ADDR']."',NOW(),NOW()+INTERVAL ".$ipban." HOUR);");
			}
			Header('Location: '.mgetOpt('url-base')."?reg=ok");
		}
	}
?>